%% discretization of system 

G = ss(A,B,C,D); %create ss model from continuous system matrices
dt = 1; %sample time
H = c2d(G,dt,'zoh'); %discretise with zero order hold
[Ad,Bd,Cd,Dd] = ssdata(H); % obtain discrete system matrices 

%% pole definitions
% desired rise time in seconds
tau_H = 500; 
tau_S = 501; % note: different tau because otherwise algebraic multiplicity of poles
% controller poles
p_C = [-1/500 -1/501 -1/500 -1/501]; %NOT SURE if still case after c2d
% observer poles, we will take these 10 times as fast as the controller
p_O = p_C/10;

L = place(A,B,p_C); %controller gain
K = place(A',C',p_O)'; %observer gain



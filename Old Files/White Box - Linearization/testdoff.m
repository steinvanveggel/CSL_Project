syms T_H1(t) T_H2(t) T_S1(t) T_S2(t)

Ta = 23; 
alpha1 = 0.01; 
alpha2 = 0.0075;
Cp = 500.0; 
A = 12 / 100^2; 
As = 2.0 / 100.0^2; 
m = 4 / 1000; 
U = 10; 
eps = 0.9; 
sigma = 5.67e-8; 
tau=24; 

Q1 = 40;
Q2 = 40; 

ode1 = diff(T_H1,t) == (1.0/(m*Cp))*(U*A*(Ta-T_H1) + (eps * sigma * A * (Ta^4 - T_H1^4)) + U*As*(T_H2-T_H1) + eps*sigma*A * (T_H2^4 - T_H1^4) + alpha1*Q1);
ode2 = diff(T_H2,t) == (1.0/(m*Cp))*(U*A*(Ta-T_H2) + (eps * sigma * A * (Ta^4 - T_H2^4)) - U*As*(T_H2-T_H1) - eps*sigma*A * (T_H2^4 - T_H1^4) + alpha2*Q2);
ode3 = diff(T_S1,t) == (1/tau)*(T_H1-T_S1);
ode4 = diff(T_S2,t) == (1/tau)*(T_H2-T_S2);

cond1 = T_H1(0) == 40;
cond2 = T_H2(0) == 40;
cond3 = T_S1(0) == 40;
cond4 = T_S2(0) == 40;

ode = [ode1 ode2 ode3 ode4]';
cond =[cond1 cond2 cond3 cond4]';
ySol(t) = dsolve(ode,cond)
close all; clear; clc
%% initialize parameters
Ta = 23; 
alpha1 = 0.01; 
alpha2 = 0.0075;
tau = 24; 
U = 10; 
Us = 10;
eps = 0.9; 
alpha1 = 0.01; 
alpha2 = 0.0075;
Cp = 500.0; 
par_int = [Ta alpha1 alpha2 tau Cp U Us eps];

%% load data
load('data.mat') % simulation data, multisine as input
load('data_real.mat'); % real data, binary noise around Q = 40 as input
load('data2.mat'); % real data, multisine around Q = 20 as input
data2 = table2array(data2);

data1 = data(1:900,:); %simulation multisine
data1(:,3) = data1(:,3) - 273.15;
data1(:,4) = data1(:,4) - 273.15;

data3 = data2; %real multisine
data2 = black_box_data; %real binary noise
% x0 = [data1(1,3) data1(1,4)]
% [par_opt_1,A,B,C,D] = white_box(data1,par_int);
% [par_opt_3,A,B,C,D] = white_box(data3,par_int);
[par_opt_3,A,B,C,D] = white_box(data3,par_int);
%% System Build
A = A;
B = B;
C = C;
D = D;

% Observer Gain 
des_poles = [-0.01 ; -0.01 ; -0.1;-0.1];
L = place( A',C',des_poles)';
% Observer Matrices
Aobs = [A - L*C];
Bobs = [B L];
Cobs = eye(4);
Dobs = zeros(4,4);








% [par_opt_2,A,B,C,D] = white_box(data2,par_int);
% figure(2)
% sgtitle('Experiment, binary noise input')
% 
% [par_opt_3,A,B,C,D] = white_box(data3,par_int);
% figure(3)
% sgtitle('Experiment, multisine input')
% 
% 
% par_int
% par_opt_1
% par_opt_2
% par_opt_3



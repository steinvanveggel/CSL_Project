function [par_opt,A,B,C,D] = white_box(data,par_int)
%% constants 
% fixed
A = 12 / 100^2; 
As = 2.0 / 100.0^2; 
m = 4 / 1000;
sigma = 5.67e-8; 
% to be optimized
Ta = par_int(1); 
alpha1 = par_int(2); 
alpha2 = par_int(3);
tau = par_int(4);
Cp = par_int(5);
U = par_int(6);
Us = par_int(7);
eps = par_int(8);

%% parameters
p_fix = [A As m sigma]; % fixed parameters
p_int = [Ta alpha1 alpha2 tau Cp U Us eps];% initial parameter values, to be optimized 
p_low = zeros(1,8); % lower bound
p_upp = 10*p_int; % upper bound

%% lsqnonlin algorithm 
func = @(p)errfun(p,p_fix,data); % error function
options = optimset('lsqnonlin');
options = optimset(options, 'Display','iter');
[p_opt,V2,~,~,output2] = lsqnonlin(func,p_int,p_low,p_upp,options); % obtain optimized parametervector
% calculate final Tmod
[~,Tmodint] = errfun(p_int,p_fix,data); % Tmod with initial parametervector
[~,Tmod] = errfun(p_opt,p_fix,data); % Tmod with optimized parametervector 

%% make linearized model
t = 1:1:900; % time vector 
Ts= 900;
eq = 40; % linearization point at 40 degrees
[Am,Bm,Cm,Dm]=linearize(p_fix,p_opt,eq); % obtain system matrices
sys = ss(Am,Bm,Cm,Dm); 
% x0 = [23 23 23 23];
A = Am;
B = Bm;
C = Cm;
D = Dm;
x0 = [data(1,3) data(1,4) data(1,3) data(1,4)]'; % initial conditions
u = [data(:,1) data(:,2)]'; % input 
y = lsim(sys,u,t,x0); % obtain output 

%% extract outputs for plotting
Tmodint_S1 = Tmodint(:,3); % Tmod with initial parametervector
Tmodint_S2 = Tmodint(:,4);
Tmod_S1 = Tmod(:,3); % Tmod with optimized parametervector 
Tmod_S2 = Tmod(:,4);
Tmodlin_S1 = y(:,1); % Tmod with optimized parametervector, linearized around 40 deg
Tmodlin_S2 = y(:,2);
Treal_S1 = data(:,3); % T from actual data
Treal_S2 = data(:,4);
Q1 = data(:,1); % Q from data
Q2 = data(:,2);

%% evaluate goodness of fit 
cost_func = 'MSE';  % mean squared error
fit_1 = goodnessOfFit(Tmod_S1,Treal_S1,cost_func); % for nonlinear model
fit_2 = goodnessOfFit(Tmod_S2,Treal_S2,cost_func); 
fitlin_1 = goodnessOfFit(Tmodlin_S1,Treal_S1,cost_func); % for linearized model 
fitlin_2 = goodnessOfFit(Tmodlin_S2,Treal_S2,cost_func); 

%% plot results
figure
subplot(221) % input 1
plot(t,Q1), hold on
xlabel('time (s)','Fontsize',14)
ylabel('input Q1 (%)','Fontsize',14)
ylim([0 100]);
subplot(222) % input 2
plot(t,Q2), hold on
xlabel('time (s)','Fontsize',14)
ylabel('input Q2 (%)','Fontsize',14)
ylim([0 100]);
subplot(223) % TS1
plot(t,Tmod_S1,'LineWidth',2), hold on
plot(t,Tmodlin_S1,'LineWidth',2)
plot(t,Tmodint_S1,'LineWidth',1)
plot(t,Treal_S1,'LineWidth',1)
xlabel('time (s)','Fontsize',14)
ylabel('TS1 (celcius)','Fontsize',14)
ylim([0 80]);
legend('TS1_{model,nonlinear}','TS1_{model,linearized}','TS1_{model,initial}','TS1_{real}')
title(['MSE of nonlinear fit = ',num2str(fit_1),',  MSE of linear fit = ',num2str(fitlin_1)])
subplot(224) % TS2
plot(t,Tmod_S2,'LineWidth',2), hold on
plot(t,Tmodlin_S2,'LineWidth',2)
plot(t,Tmodint_S2,'LineWidth',1)
plot(t,Treal_S2,'LineWidth',1)
xlabel('time (s)','Fontsize',14)
ylabel('TS2 (celsius)','Fontsize',14)
ylim([0 80]);
legend('TS2_{model,nonlinear}','TS2_{model,linearized}','TS2_{model,initial}','TS2_{real}')
title(['MSE of nonlinear fit = ',num2str(fit_2),',  MSE of linear fit = ',num2str(fitlin_2)])

%% return parameters
par_opt = p_opt;
end 




    
    
    



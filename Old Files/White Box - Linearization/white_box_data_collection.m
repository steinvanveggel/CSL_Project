clear; close all; clc

% Observation Time
Ts = 900;
%Sample Time = 1
t = 1:Ts;
t=t';

% %Generating a PRBS signal
% Q1 = idinput(Ts,'prbs',[0,1/4],[30,60]);
% u1 = iddata([],Q1,1);
% Q1 = [t , Q1];
% Q2 = idinput(Ts,'prbs',[0,1/4],[30,60]);
% u2 = iddata([],Q2,1);
% Q2 = [t , Q2];
% sim('heater.slx');
%% Generating a sum-of-sinusoids signal
[u,freq] = idinput([900 2 1],'sine',[0 1/8],[30 50]);
u1 = u(:,1);
u2 = u(:,2);
Q1 = [t , u1];
Q2 = [t , u2];
sim('heater.slx');

%% Plot the Input(Power) and Output(Sensor Temperatures)
figure(1)
subplot(211)
plot(tout,T_S1,'LineWidth',2), hold on
plot(tout,T_S2,'LineWidth',2)
xlabel('time (s)','Fontsize',14)
ylabel('state','Fontsize',14)
legend('T_S1','T_S2')
subplot(212)
plot(u1), hold on
plot(u2)
xlabel('time (s)','Fontsize',14)
ylabel('input','Fontsize',14)
legend('u1','u2')

%% Saving the input and output data in a spreadsheet
% Q1 and Q2 is 0 at time = 0
u1 = [0;u1(:,:)];
u2 = [0;u2(:,:)];
data = [u1,u2,T_S1,T_S2];
csvwrite('white_box_data.csv',data)
save('data.mat','data');

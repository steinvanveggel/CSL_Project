
function [e,Tmod] = errfun(p,p_fix,data)  
    %% input, output and initial conditions from data
    Q1 = data(:,1);
    Q2 = data(:,2);
    Treal_S1 = data(:,3);
    Treal_S2 = data(:,4);
    T_initial = [data(1,3) data(1,4) data(1,3) data(1,4)];
    
    %% solve ODE
    time = 1:1:900; % time vector 
    [~,Tmod]=ode45(@(t,Tmod)model(t,Tmod,Q1,Q2,p,p_fix),time,T_initial);
    
    % extract outputs
    Tmod_S1 = Tmod(:,3);
    Tmod_S2 = Tmod(:,4);
    
    %% calculate errors
    e_1 = abs(Tmod_S1-Treal_S1); 
    e_2 = abs(Tmod_S2-Treal_S2);
    % add errors for TS1 and TS2 
    e = e_1 + e_2;
end 

t=1:1:1800;
%% Loading the data for Black-Box identification(Linear)
% load prbs_exp_data.csv
% load prbs_val_data.csv
% load prbs_test_data.csv
% load stair_step.csv
% load a.csv
% load prbs_data_12.csv
% load prbs_data_q12_val.csv
% load prbs_data_q2.csv
% load prbs_data_q2_val.csv
% load prbs_data_12.csv
% load prbs_data_q2.csv
% load prbs_data_q1.csv
% load prbs_data_q12_val.csv
% load prbs_data_q2_val.csv
% load prbs_data_q1_val.csv
% data1 = prbs_data_12;
% data2 = prbs_data_q2;
% data3 = prbs_data_q1;
% data_val1 = prbs_data_q12_val;
% data_val2 = prbs_data_q2_val;
% data_val3 = prbs_data_q1_val;
load prbs_data_1800.csv
load prbs_data_1800val.csv
train = prbs_data_1800(:,:);
test = prbs_data_1800val(:,:);
%% Processing the training data
Q1 = train(:,1);
Q2 = train(:,2);
T_S1 = train(:,3)- train(1,3);
T_S2 = train(:,4)- train(1,4);
train_data = iddata([T_S1,T_S2],[Q1,Q2],1);
train_data.InputName  = {'Heater 1 Power';'Heater 2 Power'};
train_data.OutputName = {'Sensor 1 Temperature';'Sensor 2 Temperature'};
%% Black-Box Identification
% Define the ARMAX Model - Training
opt = armaxOptions;
opt.Focus = 'simulation';
opt.SearchMethod = 'lm';
opt.SearchOptions.MaxIterations = 50;
opt.Display = 'on';
na = [2 2;2 2];
nb = [2 2;2 2];
nc = [1;1];
nk = ones(2,2);

sys1 = armax(train_data,[na nb nc nk],opt);

%Converting ARX model to statespace
sys_ss = idss(sys1);
sys_tf = tf(sys1);
A = sys_ss.A;
B = sys_ss.B;
C = sys_ss.C;
D = sys_ss.D;
%Simulating the model using the input u
% Retrieving the data as u(Input) and y(Output)
u_train = train_data.u;
y_train = train_data.y;

%% Testing data Processing
Q1_test = test(:,1);
Q2_test = test(:,2);
T_S1_test = test(:,3)- test(1,3);
T_S2_test = test(:,4)- test(1,4);
test_data = iddata([T_S1_test,T_S2_test],[Q1_test,Q2_test],1);
test_data.InputName  = {'Heater 1 Power';'Heater 2 Power'};
test_data.OutputName = {'Sensor 1 Temperature';'Sensor 2 Temperature'};
u_test = test_data.u;
y_test = test_data.y;
y_train = lsim(sys1,u_train);
y_arx = lsim(sys1,u_test);
y_ss  = lsim(sys_ss,u_test);
y_tf = lsim(sys_tf,u_test);

%% Plotting the Real and validation data
figure(1)
subplot(2,1,1)
plot(t,y_test(:,1),'LineWidth',2), hold on
plot(t,y_arx(:,1),'LineWidth',2)
% plot(t,y_train(:,1),'LineWidth',2)
% plot(t,y_ss(:,1),'LineWidth',2)
xlabel('Time (s)','Fontsize',14)
ylabel('Temperature [^{o} C]','Fontsize',14)
legend('TS1_{real}','TS1_{model}')
subplot(2,1,2)
plot(t,y_test(:,2),'LineWidth',2), hold on
plot(t,y_arx(:,2),'LineWidth',2)
% plot(t,y_train(:,2),'LineWidth',2)
% plot(t,y_ss(:,2),'LineWidth',2)
xlabel('Time (s)','Fontsize',14)
ylabel('Temperature [^{o} C]','Fontsize',14)
legend('TS2_{real}','TS2_{model}')

%% Goodness of fit
figure(2)
compare(train_data,sys1);
figure(3)
step(sys_tf)
Fit_report1 = sys1.Report.Fit;
Tmod_S1 = y_arx(:,1);
Tmod_S2 = y_arx(:,2);
Treal_S1 = y_test(:,1);
Treal_S2 = y_test(:,2);
cost_func = 'MSE';  % mean squared error
fit_bb_S1 = goodnessOfFit(Tmod_S1,Treal_S1,cost_func); % for linearized model 
fit_bb_S2 = goodnessOfFit(Tmod_S2,Treal_S2,cost_func);

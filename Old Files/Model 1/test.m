clc
clear all
%% Loading the data for Black-Box identification(Linear)
load prbs_exp_data.csv
dt=1;        % sample frequency

t=1:900;
Q1 = prbs_exp_data(:,1);
Q2 = prbs_exp_data(:,2);
T_S1 = prbs_exp_data(:,3);
T_S2 = prbs_exp_data(:,4);
T_S1 = smooth(T_S1,0.1,'loess');
T_S2 = smooth(T_S2,0.1,'loess');
dat = iddata([T_S1,T_S2],[Q1,Q2],dt);
dat.InputName  = {'Heater 1 Power';'Heater 2 Power'};
dat.OutputName = {'Sensor 1 Temperature';'Sensor 2 Temperature'};

%% defines the system and signals
fs=1/dt;        % sample frequency
N=900;         % # of samples
T=N*dt;         % observation time
t=(0:N-1).'*dt; % time vector
u=randn(N,1);   % white noise input

% discrete system & noise system
Hc=tf(1,[0.025 0.05 1]);
Hd=c2d(Hc,dt); %%convert continuous to discrete signal with sample time dt
den=get(Hd,'den');
Hn=tf(1,den{:},dt);

% simulate system and add (filtered white) noise
y0=lsim(Hd,u,t);                 % system
n=lsim(Hn,0.001*randn(N,1),t);  % noise
y=y0+n;

%% input-output models (ARX,ARMAX,OE,BJ)
% split data into two segment
%  - one set for estimation
%  - one set for validation
date = dat(1:N/2); %%eerste helft van de data 
datv = dat(N/2+1:end); %%tweede helft van de data 

na=1:2;    % possible orders for A
nb=0:1;    % possible orders for B
nk=3:4;     % possible orders for k (delay)
NN = struc(na,nb,nk);   % make a struct with all possible combinations HANDIG
v = arxstruc(date,datv,NN);
size(v)
disp(v)
% and evaluate these combinations
% apparently this computes loss function, maar waarom moet je de eerste en
% tweede helft van de data er apart in zetten?

% add AIC & MDL
v2=v(:,1:end-1);
size(v2)
disp(v2)

% v(1,:) contains error
% v(1,end) contains N
% sum(v(2:4,:) is number of parameters (na+nb+nk)
d=sum(v(2:4,1:end-1),1);
v2(5,:)=log(v(1,1:end-1))+2*d/v(1,end) ;% AIC
v2(6,:)=v(1,1:end-1).*(1+d*log(v(1,end))/v(1,end)) ;% MDL
% twee rows toevoegen aan v2, de AIC en de MDL 
size(v2)
disp(v2)
figure
subplot(311)
plot(d,v2(1,:),'bx'), box off
ylabel('V')
subplot(312)
plot(d,v2(5,:),'bx'), box off
ylabel('AIC')
subplot(313)
plot(d,v2(6,:),'bx'), box off
ylabel('MDL')
xlabel('# parameters')

% select order
nn = selstruc(v,'mdl'); % automatic!
% v is hierbij een matrix die output bevat van de arxstruc, die wordt
% gebruikt om te evalueren welke ordes van de arx optimaal zijn voor de
% estimate
m = arx(dat,nn); % eventually fit model on all data (date&datv)
%normale structuur is arx(data, orders). met orders de rijvector met daarin
%de na nb en nk, de optimale orders voor dit model. 

% second model (=wrong model structure! just an example; may be not even that bad!)
%m2= oe(dat,nn); % choice of same orders is also arbitrary!

%% freqresp
figure
bode(Hd,m)
%bode(Hd,m,m2)

%% check model and data
figure
compare(dat,m), box off
%compare(dat,m,m2)

% next will be discussed in a later lecture
return
figure,resid(dat,m)
%figure,resid(dat,m2)

figure,resid(dat,m,'fr')
%figure,resid(dat,m2,'fr')

%% other relevant commands of the SYSTEM IDENTIFICATION TOOLBOX
return
% not yet
msid = n4sid(dat);       % subspace identification (state-space model)

figure,compare(dat,m,msid)
figure,resid(dat,msid)
figure,resid(dat,msid,'fr')

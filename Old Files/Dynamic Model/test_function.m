clear; close all; clc
dt = 0.1; 
x0 = [300 400 100 150]; %initial state
N=100;
t=dt:dt:N*dt;
Q = [zeros(1,N/2) 50*ones(1,N/4) zeros(1,N/4); zeros(1,N/2) 50*ones(1,N/2)]'; %input sequence
x = NaN(N,4);
x(1,:)=x0;
for i=1:N-1
    xnew = model_heater(dt,x(i,:),Q(i,:));
    x(i+1,:)=xnew;
end

figure(1)
subplot(211)
plot(t,x(:,1),'LineWidth',2), hold on
plot(t,x(:,2),'LineWidth',2)
plot(t,x(:,3),'LineWidth',2)
plot(t,x(:,4),'LineWidth',2)
xlabel('time (s)','Fontsize',14)
ylabel('state','Fontsize',14)
legend('T_H1','T_H2','T_S1','T_S2')
subplot(212)
plot(t,Q(:,1),'LineWidth',2), hold on
plot(t,Q(:,2),'LineWidth',2)
xlabel('time (s)','Fontsize',14)
ylabel('input','Fontsize',14)
legend('Q1','Q2')


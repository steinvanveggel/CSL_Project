function X = model_heater(dt,x,Q) 
%% constants 
% ambient temperature
Ta = 23 + 273.15; % K
% heater input
alpha1 = 0.01; % W/(heater input, 0-100)
alpha2 = 0.0075;
% heat capacity
Cp = 500.0; % J/kg-K
% surface area
A = 12 / 100^2; % m^2
As = 2.0 / 100.0^2; % Area in m^2
% mass
m = 4 / 1000; % kg (4 gm)
% heat transfer coefficient
U = 10; % W/m^2-K
eps = 0.9; % emisivity
sigma = 5.67e-8; % W/m^2-K^4 % Stefan-Boltzmann constant
tau=24; % time constant 

%% state definition and state space model 
% Temperature States
    T_H1 = x(1);
    T_H2 = x(2);
    T_S1 = x(3);
    T_S2 = x(4);
    
% Inputs
    Q1 = Q(1);
    Q2 = Q(2);
% Energy Balance
% Heat Transfer Exchange Between 1 and 2
    Qc12 = U*As*(T_H2-T_H1);
    Qr12  = eps*sigma*As * (T_H2^4 - T_H1^4);
% Nonlinear Energy Balances
    T_H1 = x(1);
    T_H2 = x(2);
    T_S1 = x(3);
    T_S2 = x(4);
    
    T_H1d = (1.0/(m*Cp))*(U*A*(Ta-T_H1) + (eps * sigma * A * (Ta^4 - T_H1^4)) + Qc12 + Qr12 + alpha1*Q1);
    T_H2d = (1.0/(m*Cp))*(U*A*(Ta-T_H2) + (eps * sigma * A * (Ta^4 - T_H2^4)) - Qc12 - Qr12 + alpha2*Q2);
    T_S1d = (1/tau)*(T_H1-T_S1);
    T_S2d = (1/tau)*(T_H2-T_S2);
    
    %% update states 
    T_H1 = T_H1+dt*T_H1d;
    T_H2 = T_H2+dt*T_H2d;
    T_S1 = T_S1+dt*T_S1d;
    T_S2 = T_S2+dt*T_S2d;
    
    X = [T_H1 T_H2 T_S1 T_S2]';
end 
    
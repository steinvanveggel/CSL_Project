%------------------system states------------%
% Th1=temp heater 1
% Th2=temp heater 1
% Ts1=temp sensor 1
% Ts2=temp sensor 2
%------------------system inputs------------%
% Q1=heater input 1
% Q2=heater input 2
% y1,y2 are out put
% A,B,C linearized system matrices 
clc; clear all;
syms Ta U eps sigma A As m cp tau alpha1 alpha2 Th1 Th2 Ts1 Ts2 Q1 Q2
f1 = (U*A*(Ta - Th1)...
     + eps*sigma*A*(Ta^4 - Th1^4) ...
     + U*As*(Th2 - Th1)...
     + eps*sigma*A*(-Th1^4 + Th2^4) ...
     + alpha1*Q1)/(m*cp);
f2 = (U*A*(Ta - Th2)...
     + eps*sigma*A*(Ta^4 - Th2^4) ...
     + U*As*(Th1 - Th2)...
     + eps*sigma*A*(-Th2^4 + Th1^4) ...
     + alpha2*Q2)/(m*cp);
f3 = (Th1 - Ts1)/tau;
f4 = (Th2 - Ts2)/tau;

f = [f1;f2;f3;f4]; %state derivative
x = [Th1 Th2 Ts1 Ts2]; %state
u = [Q1 Q2]; %input
a=jacobian(f,x); % Calculating Jacobian at steady state points
b=jacobian(f,u);
Q1=3;
Q2=3;

%fill in constants 
Ta = 23 + 273.15; % K
alpha1 = 0.01; % W/(heater input, 0-100)
alpha2 = 0.0075;
cp = 500.0; % J/kg-K
A = 12 / 100^2; % m^2
As = 2.0 / 100.0^2; % m^2
m = 4 / 1000; % kg (4 gm)
U = 10; % W/m^2-K
eps = 0.9; % emisivity
sigma = 5.67e-8; % W/m^2-K^4 % Stefan-Boltzmann constant
tau=24; % time constant 

%define equilibrium point to linearize around 
Th1=Ta;
Th2=Ta;
Ts1=Ta;
Ts2=Ta;

%define C matrix 
C = [0 0 1 0;
     0 0 0 1];
A=eval(a); %linearized A matrix at steady state point
B=eval(b); %linearized B matrix at steady state point

%define transfer function
sys=ss(A,B,C,zeros(2,2));
tf=tf(sys);
%plot system properties 
figure(1)
bode(sys)
figure(2)
step(sys)
figure(3)
impulse(sys)
function y = model_sensor(dt,T_heater,T_sensor) 
tau=24;

dTs1dt = (T_heater(1) - T_sensor(1))/tau;
dTs2dt = (T_heater(2) - T_sensor(2))/tau;

y = [dTs1;dTs2];
end
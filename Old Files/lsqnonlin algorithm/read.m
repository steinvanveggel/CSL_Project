close all; clear; clc

%% initialize parameters
alpha1 = 0.01; 
alpha2 = 0.0075;
tau=24; 
par_int = [alpha1 alpha2 tau];

%% load data
load('data.mat') % simulation data, multisine as input
load('data_real.mat'); % real data, binary noise around Q = 40 as input
% load('data2.mat'); % real data, multisine around Q = 20 as input

data1 = data(1:900,:); %simulation multisine
data1(:,3) = data1(:,3) - 273.15;
data1(:,4) = data1(:,4) - 273.15;

% data3 = data2; %real multisine
data2 = black_box_data; %real binary noise

par_opt_1 = white_box(data1,par_int);
figure(1)
sgtitle('Simulation')

par_opt_2 = white_box(data2,par_int);
figure(2)
sgtitle('Experiment, binary noise input')

par_opt_3 = white_box(data3,par_int);
figure(3)
sgtitle('Experiment, multisine input')


par_int
par_opt_1
par_opt_2
par_opt_3



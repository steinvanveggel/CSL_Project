function dTdt=model(t,Tmod,Q1,Q2,p,p_fix)
        % select temperatures 
        T_H1 = Tmod(1);
        T_H2 = Tmod(2);
        T_S1 = Tmod(3);
        T_S2 = Tmod(4);
        
        % extract inputs for this time instant
        Q1_inter = Q1(round(t));
        Q2_inter = Q2(round(t));
        
        % select parameters from vector
        % fixed parameters
        Ta = p_fix(1);
        A = p_fix(2);
        As = p_fix(3);
        m = p_fix(4);
        Cp = p_fix(5);
        U = p_fix(6);
        eps = p_fix(7);
        sigma = p_fix(8);
      
        % to be optimized 
        alpha1 = p(1);
        alpha2 = p(2);
        tau = p(3);
        
        % heat transfer between plates
        Qc12 = U*As*(T_H2-T_H1);
        Qr12  = eps*sigma*A * (T_H2^4 - T_H1^4);
        
        % diff equations    
        T_H1d = (1.0/(m*Cp))*(U*A*(Ta-T_H1) + (eps * sigma * A * (Ta^4 - T_H1^4)) + U*As*(T_H2-T_H1) + Qr12 + alpha1*Q1_inter);
        T_H2d = (1.0/(m*Cp))*(U*A*(Ta-T_H2) + (eps * sigma * A * (Ta^4 - T_H2^4)) - Qc12 - Qr12 + alpha2*Q2_inter);
        T_S1d = (1/tau)*(T_H1-T_S1);
        T_S2d = (1/tau)*(T_H2-T_S2);
        
        dTdt = [T_H1d  T_H2d  T_S1d  T_S2d]';
    end 

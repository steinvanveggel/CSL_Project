%% Assignment 3, Question 1: Multivariable system identification
% template SIPE [ME41065]
%
% You can use this template to answer the assignment. When you see 000 or
% '..' you need to enter your own code.
close all
clear
clc

%%
% Variables
fs  = 100;                  % sample frequency
T   = 80;                   % Total time
N   = T*fs;                  % number of samples
t   = (0:N-1)'/fs;                  % Time vector


% Make 2 iNLepeNLent input signals
u_in=randn(N,2);
u1_in = [t, u_in(:,1)];     % input signal 1
u2_in = [t, u_in(:,2)];     % input signal 2

% Simulate MIMO system to generate output data (y1 & y2)
load_system('Ass3_MIMOopen')
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','LowerValue','0'); %g/h
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','UpperValue','0'); %g/h
set_param('Ass3_MIMOopen/Subsystem/H11/Noise','Variance','0.05');
set_param('Ass3_MIMOopen/Subsystem/H12/Noise','Variance','0.005');
set_param('Ass3_MIMOopen/Subsystem/H21/Noise','Variance','0.1');
set_param('Ass3_MIMOopen/Subsystem/H22/Noise','Variance','0.001');
options=simset('OutputPoints','specified');
sim('Ass3_MIMOopen',t,options);


%% Question A-B

% Welch averaging
L=8;            % # of segments
NL=floor(N/L);  % # of samples per segment

% Spectral densities 
Sy1u1 = cpsd(y1,u1,hann(NL),[],NL,fs);
Sy1u2 = cpsd(y1,u2,hann(NL),[],NL,fs); %cross

Su2u2 = cpsd(u2,u2,hann(NL),[],NL,fs);
Su1u1 = cpsd(u1,u1,hann(NL),[],NL,fs); 
Sy1y1 = cpsd(y1,y1,hann(NL),[],NL,fs); %auto

Su2u1 = cpsd(u2,u1,hann(NL),[],NL,fs);
Su1u2 = cpsd(u1,u2,hann(NL),[],NL,fs); %cross inputs

% ---Uncorrected---
Hy1u1=Sy1u1./Su1u1;
Hy1u2=Sy1u2./Su2u2;

% ---Corrected---
[H11,H21,H12,H22,f]=tfestmimo(u1,u2,y1,y2,L,fs);

% ---Htrue---
SYSy1u1 = tf([1],[0.02 0.05 1]);  
SYSy1u2 = tf([1],[ 0.02 1]);        
[fSYSy1u1,freqgrid] = freqresp(SYSy1u1);
[fSYSy1u2,freqgrid2]=freqresp(SYSy1u2);     
Htrue_y1u1 = squeeze(fSYSy1u1);     
Htrue_y1u2 = squeeze(fSYSy1u2);

% ---Plot---
figure('Name','Question 1a-b')
subplot(221) % Hy1u1 magnitude
loglog(f,abs(Hy1u1)), hold on 
loglog(f,abs(H11))            
loglog(freqgrid/(2*pi),abs(Htrue_y1u1))
legend('Huncor','Hcor','Htrue');
ylabel('Magnitude');
title('Hy1u1')

subplot(223) % Hy1u1 phase 
semilogx(f,angle(Hy1u1)), hold on
semilogx(f,angle(H11))       
semilogx(freqgrid/(2*pi),angle(Htrue_y1u1))    
legend('Huncor','Hcor','Htrue');
ylabel('Phase [rad]');

subplot(222) % Hy1u2 magnitude
loglog(f,abs(Hy1u2)), hold on 
loglog(f,abs(H12))            
loglog(freqgrid2/(2*pi),abs(Htrue_y1u2))
legend('Huncor','Hcor','Htrue');
ylabel('Magnitude');
title('Hy1u2')

subplot(224) % Hy1u2 phase 
semilogx(f,angle(Hy1u2)), hold on
semilogx(f,angle(H12))       
semilogx(freqgrid2/(2*pi),angle(Htrue_y1u2))    
legend('Huncor','Hcor','Htrue');
ylabel('Phase [rad]');
xlabel('Frequency [Hz]')

%% Question C: Ordinary Coherence
% Calculate spectral densities (for example Sy1u1 = cpsd(y1,u1,hann(NL),[],NL,fs));
[Cy1u1,fc1]=mscohere(u1,y1,hann(NL),[],NL,fs); 
[Cy1u2,fc2]=mscohere(u2,y1,hann(NL),[],NL,fs); 

% Plot
figure('Name','Question 1c')
semilogx(fc1,Cy1u1), hold on
semilogx(fc2,Cy1u2)
legend('y1u1','y1u2');
ylabel('Coherence');
xlabel('Frequency [Hz]')
title('Ordinary coherences')

%% Question D: Multiple Coherence
Cy1u=Cy1u1+Cy1u2; %simply summing ordinary coherences

Cy1u_cor=real((Sy1u1.*conj(Sy1u1./Su1u1)+Sy1u2.*conj(Sy1u2./Su2u2))./(Sy1y1)); %actual formula for multiple coherence
Cy1u_cor=real((Sy1u1.*conj(H11)+Sy1u2.*conj(H12))./(Sy1y1)); %actual formula for multiple coherence

figure('Name','Question 1d')
semilogx(fc2,Cy1u), hold on 
semilogx(fc2,Cy1u_cor)
legend('Summed ordinary','Multiple'); 
ylabel('Coherence');
xlabel('Frequency [Hz]')
title('Difference between summed and multiple coherence');

%% Question E: Partial Coherence


Cu1u2 = mscohere(u1,u2,hann(NL),[],NL,fs);
Cu2u1 = mscohere(u2,u1,hann(NL),[],NL,fs);
Cy1u2 = mscohere(y1,u2,hann(NL),[],NL,fs);
Cy1u1 = mscohere(y1,u1,hann(NL),[],NL,fs);

Cy1u1Du2=(abs(Sy1u1.*Su2u2-Sy1u2.*Su2u1).^2)./((Su2u2.^2.*Su1u1.*(1-Cu1u2).*Sy1y1.*(1-Cy1u2)));
Cy1u2Du1=(abs(Sy1u2.*Su1u1-Sy1u1.*Su1u2).^2)./((Su1u1.^2).*Su2u2.*(1-Cu2u1).*Sy1y1.*(1-Cy1u1));

figure('Name','Question 1e')
semilogx(fc2,Cy1u1Du2), hold on 
semilogx(fc2,Cy1u2Du1)
legend('y1u1','y1u2');
xlabel('Frequency [Hz]')
ylabel('Coherence')
title('Partial coherences')

%% Question F: Deadzone
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','LowerValue','-0.3');
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','UpperValue','0.3');
options=simset('OutputPoints','specified');
sim('Ass3_MIMOopen',t,options);

% Recalculate transfer functions
[H11,H21,H12,H22,f]=tfestmimo(u1,u2,y1,y2,L,fs);

% Recalculate closed loop spectral estimator
Su1u1 = cpsd(u1,u1,hann(NL),[],NL,fs);
Su2u2 = cpsd(u2,u2,hann(NL),[],NL,fs);
Sy1y1 = cpsd(y1,y1,hann(NL),[],NL,fs);
Sy2y2 = cpsd(y2,y2,hann(NL),[],NL,fs); %autospectral

Su1u2 = cpsd(u1,u2,hann(NL),[],NL,fs);
Su2u1 = cpsd(u2,u1,hann(NL),[],NL,fs); %cross spectral inputs

Sy1u1 = cpsd(y1,u1,hann(NL),[],NL,fs);
Sy1u2 = cpsd(y1,u2,hann(NL),[],NL,fs);
Sy2u1 = cpsd(y2,u1,hann(NL),[],NL,fs);
Sy2u2 = cpsd(y2,u2,hann(NL),[],NL,fs); %cross spectral inputs and outputs

% Ordinary coherence
Cy1u2 = mscohere(y1,u2,hann(NL),[],NL,fs);
Cy1u1 = mscohere(y1,u1,hann(NL),[],NL,fs);
Cy1u_mul=real((Sy1u1.*conj(H11)+Sy1u2.*conj(H12))./(Sy1y1));

% Partial coherence
Cy1u1Du2=(abs(Sy1u1.*Su2u2-Sy1u2.*Su2u1).^2)./((Su2u2.^2.*Su1u1.*(1-Cu1u2).*Sy1y1.*(1-Cy1u2)));
Cy1u2Du1=(abs(Sy1u2.*Su1u1-Sy1u1.*Su1u2).^2)./((Su1u1.^2).*Su2u2.*(1-Cu2u1).*Sy1y1.*(1-Cy1u1));

figure('Name','Question 1f')
semilogx(f,Cy1u_mul), hold on 
semilogx(f,Cy1u1Du2)
semilogx(f,Cy1u2Du1)
legend('Multiple','Partial1','Partial2');
ylabel('Coherence');
xlabel('Frequency [Hz]');
title('Coherences with increased dead zone');

%% Question G: MIMO open loop
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','LowerValue','0');
set_param('Ass3_MIMOopen/Subsystem/H11/Dead Zone','UpperValue','0');
set_param('Ass3_MIMOopen/Subsystem/H11/Noise','Variance','0.05');
set_param('Ass3_MIMOopen/Subsystem/H12/Noise','Variance','0.005');
set_param('Ass3_MIMOopen/Subsystem/H21/Noise','Variance','0.1');
set_param('Ass3_MIMOopen/Subsystem/H22/Noise','Variance','0.001');
options=simset('OutputPoints','specified');
sim('Ass3_MIMOopen',t,options);

% Closed loop estimators
[H11,H21,H12,H22,f]=tfestmimo(u1,u2,y1,y2,L,fs);

% Spectral densities Y1
Su1u1 = cpsd(u1,u1,hann(NL),[],NL,fs);
Su2u2 = cpsd(u2,u2,hann(NL),[],NL,fs);
Sy1y1 = cpsd(y1,y1,hann(NL),[],NL,fs);
Sy2y2 = cpsd(y2,y2,hann(NL),[],NL,fs); %autospectral

Su1u2 = cpsd(u1,u2,hann(NL),[],NL,fs);
Su2u1 = cpsd(u2,u1,hann(NL),[],NL,fs); %cross spectral inputs

Sy1u1 = cpsd(y1,u1,hann(NL),[],NL,fs);
Sy1u2 = cpsd(y1,u2,hann(NL),[],NL,fs);
Sy2u1 = cpsd(y2,u1,hann(NL),[],NL,fs);
Sy2u2 = cpsd(y2,u2,hann(NL),[],NL,fs); %cross spectral inputs and outputs

% Ordinary coherence Y1
Cy1u2 = mscohere(y1,u2,hann(NL),[],NL,fs);
Cy1u1 = mscohere(y1,u1,hann(NL),[],NL,fs);
Cy1u_mul=real((Sy1u1.*conj(H11)+Sy1u2.*conj(H12))./(Sy1y1));

% Partial coherence Y1
Cy1u1Du2=(abs(Sy1u1.*Su2u2-Sy1u2.*Su2u1).^2)./((Su2u2.^2.*Su1u1.*(1-Cu1u2).*Sy1y1.*(1-Cy1u2)));
Cy1u2Du1=(abs(Sy1u2.*Su1u1-Sy1u1.*Su1u2).^2)./((Su1u1.^2).*Su2u2.*(1-Cu2u1).*Sy1y1.*(1-Cy1u1));




% Spectral densities Y2
Su1u1 = cpsd(u1,u1,hann(NL),[],NL,fs);
Su2u2 = cpsd(u2,u2,hann(NL),[],NL,fs);
Sy1y1 = cpsd(y1,y1,hann(NL),[],NL,fs);
Sy2y2 = cpsd(y2,y2,hann(NL),[],NL,fs); %autospectral

Su1u2 = cpsd(u1,u2,hann(NL),[],NL,fs);
Su2u1 = cpsd(u2,u1,hann(NL),[],NL,fs); %cross spectral inputs

Sy1u1 = cpsd(y1,u1,hann(NL),[],NL,fs);
Sy1u2 = cpsd(y1,u2,hann(NL),[],NL,fs);
Sy2u1 = cpsd(y2,u1,hann(NL),[],NL,fs);
Sy2u2 = cpsd(y2,u2,hann(NL),[],NL,fs); %cross spectral inputs and outputs

% Ordinary coherence Y2
Cy2u2 = mscohere(y2,u2,hann(NL),[],NL,fs);
Cy2u1 = mscohere(y2,u1,hann(NL),[],NL,fs);
Cy2u_mul=real((Sy2u1.*conj(H21)+Sy2u2.*conj(H22))./(Sy2y2));

% Partial coherence Y2
Cy2u1Du2=(abs(Sy2u1.*Su2u2-Sy2u2.*Su2u1).^2)./((Su2u2.^2.*Su1u1.*(1-Cu1u2).*Sy2y2.*(1-Cy2u2)));
Cy2u2Du1=(abs(Sy2u2.*Su1u1-Sy2u1.*Su1u2).^2)./((Su1u1.^2).*Su2u2.*(1-Cu2u1).*Sy2y2.*(1-Cy2u1));

figure('Name','Question 1g')
subplot(321) % Hy1u: Hy1u1 & Hy1u2
loglog(f,abs(H11)),hold on           
loglog(f,abs(H12))      
legend('Hy1u1,Hy1u2');
ylabel('Magnitude');
title('Hy1u')
subplot(323) % Hy1u1 phase 
semilogx(f,angle(H11)), hold on       
semilogx(f,angle(H12))       
legend('Hy1u1,Hy1u2');
ylabel('Phase [rad]')
subplot(325)
semilogx(f,Cy1u_mul), hold on 
semilogx(f,Cy1u1Du2)
semilogx(f,Cy1u2Du1)
legend('Multiple','Partial1','Partial2')
ylabel('Coherence');
xlabel('Frequency [Hz]')

subplot(322) % Hy2u: Hy1u1 & Hy2u2
loglog(f,abs(H21)),hold on           
loglog(f,abs(H22))      
legend('Hy2u1,Hy2u2');
ylabel('Magnitude');
title('Hy2u')
subplot(324) % Hy2u1 phase 
semilogx(f,angle(H21)), hold on       
semilogx(f,angle(H22))       
legend('Hy2u1,Hy2u2');
ylabel('Phase [rad]')
subplot(326)
semilogx(f,Cy2u_mul), hold on 
semilogx(f,Cy2u1Du2)
semilogx(f,Cy2u2Du1)
legend('Multiple','Partial1','Partial2')
ylabel('Coherence');
xlabel('Frequency [Hz]')

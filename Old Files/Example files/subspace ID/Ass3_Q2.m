%% Assignment 3, Question 2: Input signal design
% template SIPE [ME41065]
%
% You can use this template to answer the assignment. When you see 000 or
% '..' you need to enter your own code.
clear; close all; clc

% Variables
fs  = 200; % sample frequency
dt  =1/fs; 
T   = 200;                  % Total time
N   = T*fs;
tv   = (0:N-1)'/fs;        % Time vector
fv  = (0:N-1)'/N*fs;        % frequency vector [Hz]

%% Question A: design the input signals

maxAmp  = 1.5;
fcut    = 40;       % [Hz]
L = 10;

% signal A: filtered white noise

% Generate filter
[b,a]=butter(2,fcut*2/fs);  % second order butterworth low pass filter
% Create white noise
x_wn = randn(N,1);
% Filter noise
uA=filtfilt(b,a,x_wn);

% Scale and determine max, std and crest factor
uA=maxAmp*uA/max(uA);
stdA=std(uA);
maxA=max(uA);
crestA=maxA/stdA;


% signal B: multisine [1/T,fcut] Hz, random phase
Tm  = T/L; %time of one multisine recording
NL  = Tm/dt; %number of samples in one recording 
fm  = (0:NL-1)'/Tm; %frequency vector in one recording 

% signal B: random phase multisine

fpower = 1/Tm:1/Tm:40;     % Put power at these frequencies [Hz] (corrected to be multiples of 4 bands)
apower = exp(2*pi*1j*rand(size(fpower)));            % amplitude of 1 with random phase
[uB, ~] = msinprep(fpower,apower,NL,fs,'screen');
uB=repmat(uB,L,1);


% Scale and determine max, std and crest factor
uB=maxAmp*uB/max(uB);
stdB=std(uB);
maxB=max(uB);
crestB=maxB/stdB;

% signal C: crested version of signal B
Ccrest = msinclip(fpower, apower, [], 'nograph');
[uC, ~] = msinprep(fpower,Ccrest,NL,fs,'screen');
uC=repmat(uC,L,1);



% Scale and determine max, std and crest factor
uC=maxAmp*uC/max(uC);
stdC=std(uC);
maxC=max(uC);
crestC=maxC/stdC;

% Plot signals
figure('Name','Question A Signals in Time Domain')
subplot(311)
plot(tv,uA); title('SigA'); ylabel('[-]')
subplot(312)
plot(tv,uB); title('SigB'); ylabel('[-]')
subplot(313)
plot(tv,uC); title('SigC'); xlabel('Time [s]'); ylabel('[-]')

% Calculate power spectral density for each signal

f = 0:fs/N:fs/2;
% UA=fft(uA);
% UA=UA(1:N/2+1);
% SuuA=1/N*(UA.*conj(UA));
% % 
% UB=fft(uB);
% UB=UB(1:N/2+1);
% SuuB=1/N*abs(UB).^2;
% % 
% UC=fft(uC);
% UC=UC(1:N/2+1);
% SuuC=1/N*(UC.*conj(UC));

% % 
SuuA=(1/N).*(abs(fft(uA)).^2);
SuuA=SuuA(1:N/2+1);

SuuB=(1/N).*(abs(fft(uB)).^2);
SuuB=SuuB(1:N/2+1);

SuuC=(1/N).*(abs(fft(uC)).^2);
SuuC=SuuC(1:N/2+1);

%Plot
figure('Name','Question A Signals in Frequency Domain')
plot(f,abs(SuuC)), hold on 
plot(f,abs(SuuB))
plot(f,abs(SuuA))
legend('crested multisine','multisine','colored noise')
title('Suu');
 
%TABLE
table_results=[maxA, maxB, maxC;
    stdA, stdB, stdC;
    crestA, crestB, crestC;
    1/T 1/Tm 1/Tm];

display(table_results)

%% Question A: simulate the system

% run the model
options=simset('OutputPoints','specified'); % only produce output for the time values in vector <t>
% Sig A
u_in = [tv uA];
sim('Ass3_SISOopen',tv,options);
yA = y; clear u_in y
% Sig B
u_in = [tv uB];
sim('Ass3_SISOopen',tv,options);
yB = y; clear u_in y
% Sig C
u_in = [tv uC];
sim('Ass3_SISOopen',tv,options);
yC = y; clear u_in y

% Plot signals
figure('Name','Question B Signals in Time Domain')
subplot(311)
plot(tv,yA); title('SigA'); ylabel('[-]')
subplot(312)
plot(tv,yB); title('SigB'); ylabel('[-]')
subplot(313)
plot(tv,yC); title('SigC'); xlabel('Time [s]'); ylabel('[-]')

% spectral densities (welch averaged)
L=10;            % # of segments
NL=floor(N/L);  % # of samples per segment

[SyyA,f] = pwelch(yA,rectwin(NL),0,NL,fs);
SyyB = pwelch(yB,rectwin(NL),0,NL,fs);
SyyC = pwelch(yC,rectwin(NL),0,NL,fs);



% plot
figure('Name','Question 1b Signals in Frequency Domain')
plot(f,abs(SyyA)), hold on 
plot(f,abs(SyyB))
plot(f,abs(SyyC))

legend('colored noise','multisine','crested multisine')
title('Syy'); xlabel('Frequency [Hz]'); ylabel('[-]')

%% Question C: estimate the system

% Transfer function & Coherence
% SigA
[HA,f]=tfestimate(uA,yA,hann(NL),[],NL,fs);
HB=tfestimate(uB,yB,hann(NL),[],NL,fs);
HC=tfestimate(uC,yC,hann(NL),[],NL,fs);

CA=mscohere(uA,yA,hann(NL),[],NL,fs);
CB=mscohere(uB,yB,hann(NL),[],NL,fs);
CC=mscohere(uC,yC,hann(NL),[],NL,fs);

% Htrue
s = 2*pi*1j*fv;
Htrue = 1./(0.02*s.^2+0.05*s+1);

% Auto spectral density (Welch)
SuuB = pwelch(uB,rectwin(NL),0,NL,fs);
    
% select relevant freqs
n=find(SuuB>mean(SuuB));

% Plot H&C
figure('Name','Question C')
subplot(311)
loglog(f,abs(HA),'linewidth',2); hold on;
loglog(f(n),abs(HB(n)),'r','linewidth',2);
loglog(f(n),abs(HC(n)),'g','linewidth',2);
loglog(fv,abs(Htrue),'k--','linewidth',2);
legend('SigA','SigB','SigC','Htrue','location','southwest');
ylabel('Magnitude'); xlim([0.2 fs/2]);ylim([0 20])
subplot(312)
semilogx(f,angle(HA)*180/pi,'linewidth',2); hold on;
semilogx(f(n),angle(HB(n))*180/pi,'r','linewidth',2);
semilogx(f(n),angle(HC(n))*180/pi,'g','linewidth',2);
semilogx(fv,angle(Htrue)*180/pi,'k--','linewidth',2);
ylabel('Phase [deg]'); xlim([0.2 fs/2]);
subplot(313)
semilogx(f,CA,'linewidth',2); hold on;
semilogx(f(n),CB(n),'r','linewidth',2)
semilogx(f(n),CC(n),'g','linewidth',2)
xlabel('Frequency [Hz]'); ylabel('Coherence');
xlim([0.2 fs/2]); ylim([0 1.1]);

function [ H ] = HankelMulti(X,s)
% Makes a block Hankel matrix of signal X. The rows of correspond to the
% number of -input or output- signals and the columns are the data samples.
% The variable s determines how many block rows the block matrix will have.

[N,nsig] = size(X);
% note the orientation of matrix X: signals are stored in the columns,
% samples in the number of rows (so the matrix is typically long (N) and
% narrow (nsig)

H = NaN(nsig*s,N-s+1);

for kk = 0:s-1
    H(1+kk*nsig:nsig+kk*nsig,:) = transpose(X(kk+1:end-s+kk+1,:));
    % note the transpose! H is typically wide and short (i.e. differently
    % oriented than X !!!)
end


end


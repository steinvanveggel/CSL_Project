%% Assignment 3, Question 4: Subspace units
% template SIPE [ME41065]
%
% You can use this template to answer the assignment. When you see 000 or
% '...' you need to enter your own code.
clear
close all
clc

%% System used for identification
load('data_real.mat');
data = black_box_data;

Nt=900;                                % No. Samples of simulation
dt=1;                                % Sample time
t=(0:Nt-1).'*dt;                        % Time vector
plotcol='g';                            % Plot color

% % Build coupled mass spring damper system
% m1=0.1;                                  % System parameters
% b1=0.6;
% k1=450;
% 
% m2 = 0.3;
% b2 = 0.5;
% k2 = 500;
% 
% M = [m1 0; 0 m2];
% B = [b1+b2 -b2; -b2 b2];
% K = [k1+k2 -k2; -k2 k2];
% 
% s = tf('s');
% 
% Hinv = M*s^2+B*s+K;
% 
% SYS =c2d(ss(1/Hinv,'minimal'),dt); % discrete system representation

% White noise as input
u1 = data(:,1);               % White noise input
u2 = data(:,2);
In = [u1 u2];

x0 = [data(1,3) data(1,4) data(1,3) data(1,4)] ;                % initial condition
y0=[data(:,3) data(:,4)];           % clean output (noise free)
yk=y0;   % output noise added

l= 2;                           % number of outputs
r= 2;                           % number of inputs

figure(1)
set(gcf,'name','Output time domain')
subplot(211)
plot(t,y0(:,1),t,yk(:,1),'LineWidth',2),box off, hold on;
xlabel('time (s)','FontSize',14)
ylabel('output_1 (-)','FontSize',14)
subplot(212)
plot(t,y0(:,2),t,yk(:,2),'LineWidth',2),box off, hold on;
xlabel('time (s)')
ylabel('output_2 (-)','FontSize',14)
legend('y_0','y_{noise}','FontSize',14)

% snr(000)  % Give the signal to noise ratio, output 1
% snr(000)  % Give the signal to noise ratio, output 2

%% Question A
s=5;                                % Hankel matrix starting at yk(ii) size s*r x N
ii=0;
N=Nt;    

YsN=hankel(yk(ii+1:ii+1+s-1),yk(ii+s:ii+N+s-1));    % Output Hankel matrix
UsN=hankel(In(ii+1:ii+1+s-1),In(ii+s:ii+N+s-1));    % Input Hankel matrix

% Build the correct Hankel matrices with your own function
YsN = HankelMulti(yk,s);
UsN = HankelMulti(In,s);

%% Question B

% Build the orthogonal projection matrix
PI =  eye(896)-(UsN')*(inv(UsN*(UsN')))*UsN;

YY = YsN*PI;
[U,V,S] = svd(YY);

% Check the column space to find the order of the system
figure
semilogy(diag(V),'*'), box off
title('Singular values of YsN*Phi')
ylabel('Singular values','FontSize',14)
xlabel('Model order','FontSize',14)
xlim([0 length(diag(V))+1])

%% Question C

n = 4; % Select order
Un=U(:,1:n);                        % Reduced output singular vectors
Vn=V(:,1:n);                        % Reduced input singular vectors
Sn=S(1:n,1:n);                      % Reduced singular value matrix

% Extract the system matrices
At = Un(1:(s-1)*l,:)\Un(l+1:s*l,:);
A0=SYS.a; % to check 

Ct = Un(1:l,:);

%% Question D&E

uk=In';
Phik=zeros(N*l,n+(n+l)*r);
for kk=1:N-1
    clear dummykron
    dummykron=zeros((kk)*l,n*r);
    dummysum=reshape([ones(1,kk);zeros(l-1,kk)],l*kk,1);
    dummyprod=toeplitz(dummysum(1:l),dummysum(1:l*kk));
    for tau=0:kk-1
        dummykron(tau*l+1:(tau+1)*l,:)=kron(uk(:,tau+1)',Ct*At^(kk-tau-1));
    end
    Phik(kk*l+1:(kk+1)*l,:)=[Ct*At^kk dummyprod*dummykron kron(uk(:,kk+1)',eye(l))];
end

yk2=yk(:,2)';
yk1=yk(:,1)';




theta=((1/N)*(Phik'*Phik))\(1/N*Phik')*reshape([yk1;yk2],[],1);
x0t=theta(1:n);
Bt=reshape(theta(n+1:n+n*r),n,r);
Dt=reshape(theta(n+n*r+1:n+n*r+r*l)',l,r);

%% Question F

SysId = ss(At,Bt,Ct,Dt,dt);

yID = lsim(SysId,In,t,x0t);

%computing SNR and VAF 
% VAF=vaf(yk,yID)
snr_2=snr(yk2,1e-3*randn(size(yk2)))

snr_1=snr(yk1,1e-3*randn(size(yk1)))

figure
subplot(211)
plot(t,y0(:,1),t,yID(:,1),'LineWidth',2),box off, hold on;
xlabel('time (s)','FontSize',14)
ylabel('output_1 (-)','FontSize',14)
subplot(212)
plot(t,y0(:,2),t,yID(:,2),'LineWidth',2),box off, hold on;
xlabel('time (s)')
ylabel('output_2 (-)','FontSize',14)
legend('y_0','y_{ID}','FontSize',14)


%% Question G&H

% R=triu(qr([UsN; YsN].')).';
% R22=R(000);
% 
% [U1,S1,~]=svd(R22);
% 
% figure
% semilogy(diag(S1),'*'), box off
% xlim([0 length(diag(S1))+1])
% 
% Un2= 000; 
% At = 000;
% Ct = 000;
% 
% Bt = 000;
% Dt = 000;
% 
% % Simulate ID'd system
% SysId = ss(At,Bt,Ct,Dt,dt);
% 
% yID = lsim(SysId,In,t,x0t);
% 
% figure
% subplot(211)
% plot(t,y0(:,1),t,yID(:,1),'LineWidth',2),box off, hold on;
% xlabel('time (s)','FontSize',14)
% ylabel('output_1 (-)','FontSize',14)
% subplot(212)
% plot(t,y0(:,2),t,yID(:,2),'LineWidth',2),box off, hold on;
% xlabel('time (s)')
% ylabel('output_2 (-)','FontSize',14)
% legend('y_0','y_{ID}','FontSize',14)
% 
% vaf(yk,yID)

%% extra-extra
% check the computation time of both methods (using tic ,... ,toc) for
% several data lengths (use a loop) and show how computation increases with
% data size 

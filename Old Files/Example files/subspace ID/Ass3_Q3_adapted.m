%% Assignment 3, Question 3: Subspace, Identification of autonomous systems
% template SIPE [ME41065]
%
% You can use this template to answer the assignment. When you see 000 or
% '..' you need to enter your own code.
clear; close all; clc

%% A. create system & data
% defaults
Nt=900;                                % No. Samples of simulation
dt=1;                                % Sample time
t=(0:Nt-1).'*dt;                        % Time vector
plotcol='b';                            % Plot color

<<<<<<< HEAD
load('data_real.mat');
data = black_box_data;
=======
% system
M=0.1;B=0.6;K=450;                      % System parameters
M_new=0.1;B_new=0.6;K_new=450;          % System parameters
>>>>>>> main


% system
% M=0.1;B=0.6;K=450;                      % System parameters
% M_new=0.1;B_new=0.6;K_new=450;                      % System parameters
% 
% sys0=c2d(ss(tf(1,[M B K])),dt);         % discrete system representation
% sys0_new=c2d(ss(tf(1,[M_new B_new K_new])),dt);         % different system 
% 
% A0=sys0.a;B0=sys0.b;C0=sys0.c;D0=sys0.d;

% simulation
uk=[data(:,1) data(:,2)];                        % zero input

yk=[data(:,3) data(:,4)];     % output 1

l=size(yk,2);                           % number of outputs
r=size(uk,2);                           % number of inputs
<<<<<<< HEAD
=======
%% 
figure(1)
set(gcf,'name','Output time domain')
plot(t,y0,'LineWidth',2), box off, hold on;
plot(t,yk_new,'LineWidth',1)
xlabel('time (s)','Fontsize',14)
ylabel('output (-)','Fontsize',14)
legend('y_0','y_{noise}')
>>>>>>> main

%% B. Data matrices
s=5;ii=0;N=200;                                    % Hankel matrix starting at yk(ii) size s x N
YsN=hankel(yk(ii+1:ii+1+s-1),yk(ii+s:ii+N+s-1));    % Output Hankel matrix 1
UsN=hankel(uk(ii+1:ii+1+s-1),uk(ii+s:ii+N+s-1));    % Input Hankel matrix

figure(2)
set(gcf,'name','Data Space')
plot3(YsN(1,:),YsN(2,:),YsN(3,:),'b','linewidth',2), box off, hold on

% plot3(YsN_2(1,:),YsN_2(2,:),YsN_2(3,:),'r','linewidth',2)
% plot3(YsN_3(1,:),YsN_3(2,:),YsN_3(3,:),'m','linewidth',2)
% legend('x0=[1 0]','x0=[1 1]','x0=[0 1]')

%% C. Column space Os + SVD
[U,S,V]=svd(YsN,'econ');           % Singular value decomposition
Us=U*S; 

figure(2)
for jj=1:s
    line([0 Us(1,jj)],[0 Us(2,jj)],[0 Us(3,jj)],'color','b','linestyle','--','linewidth',2)

%     line([0 Us_2(1,jj)],[0 Us_2(2,jj)],[0 Us_2(3,jj)],'color','r','linestyle','--','linewidth',2)
%     line([0 Us_3(1,jj)],[0 Us_3(2,jj)],[0 Us_3(3,jj)],'color','m','linestyle','--','linewidth',2)

end
 
figure(3)
semilogy(S,'b','marker','*'), box off, hold on
legend('Without added noise','With added noise')

xlim([0 s+1])

xlabel('singular value #','Fontsize',14)
ylabel('singular value','FontSize',14)

%% D. Retrieve system from SVD
n=2;                              % System order
Un=U(:,1:n);                        % Reduced output singular vectors
Vn=V(:,1:n);                        % Reduced input singular vectors
Sn=S(1:n,1:n);                      % Reduced singular value matrix
SnVtn=Sn*Vn';

Aid = Un(1:(n-1)*l,:)\Un(l+1:n*l,:);
Bid=SnVtn(:,1:r);
Cid=Un(1:l,:);
Did=zeros(2,2);
sysid=ss(Aid,Bid,Cid,Did,dt);       % Identified system out of SVD


%% E. plot end result
x0id=SnVtn(:,1); % with Un=[C;CA];
yi=lsim(sysid,zeros(size(t)),t,x0id);

figure(1)
plot(t,yi,'r--','LineWidth',3)
legend('y_0','y_{noise}','y_{id}')


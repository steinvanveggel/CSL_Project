close all; clear; clc
%% Initialize parameters
% To be optimized
Ta = 23; 
alpha1 = 0.01; 
alpha2 = 0.0075;
tau = 24; 
U = 10; 
Us = 10;
eps = 0.9; 
alpha1 = 0.01; 
alpha2 = 0.0075;
Cp = 500.0; 
% Fixed Parameters
A = 12 / 100^2; 
As = 2.0 / 100.0^2; 
m = 4 / 1000;
sigma = 5.67e-8;
% Initial conditions for Parameters being optimized
par_int = [Ta alpha1 alpha2 tau Cp U Us eps]; 
% Fixed parameters
par_fix = [A As m sigma]; 
%% load data
load data_exp_bn1.csv   % Experiment data, binary noise(0-40)
load data_exp_ms1.csv   % Experiment data, Multisine(0-40)
load data_sim_bn.csv    % Simulation data, binary noise(0-40)
load data_sim_ms.csv    % Simulation data, Multisine(0-40)
load prbs_val_040.csv   % Validation data, binary noise (0-40)
load multsine_val_040.csv % Validation data, Multisine (0-40)

data1 = data_exp_bn1;
data2 = data_exp_ms1;
data3 = data_sim_bn(2:end,:);
data4 = data_sim_ms(2:end,:);
data5 = prbs_val_040;
data6 = multsine_val_040; 

%% White Box Parameter Optimization
[Tmod1, par_opt_1] = white_box(data1,par_int);
figure(1)
sgtitle('Experiment, Pseudorandom Binary Sequence Input')

[Tmod2, par_opt_2] = white_box(data2,par_int);
figure(2)
sgtitle('Experiment, Multi-Sine Input')

[Tmod3, par_opt_3] = white_box(data3,par_int);
figure(3)
sgtitle('Simulation, Pseudorandom Binary Sequence Input')

[Tmod4, par_opt_4] = white_box(data4,par_int);
figure(4)
sgtitle('Simulation, Multi-Sine Input')

%% Validation of binary noise sequence  
[~,Tmod_val_bn]=errfun(par_opt_1,par_fix,data5); % simulate validation input for binary noise, with parameters obtained from simulation
[~,Tmod_val_ms]=errfun(par_opt_2,par_fix,data6); % simulate validation input for multisine, with parameters obtained from simulation


% binary noise test data 
Treal1_S1=data1(:,3);
Treal1_S2=data1(:,4);
% binary noise model 
Tmod1_S1=Tmod1(:,1);
Tmod1_S2=Tmod1(:,2);
% binary noise validation data 
Q1_bn_val = data5(:,1);
Q2_bn_val = data5(:,2);
TS1_bn_val = data5(:,3);
TS2_bn_val = data5(:,4);
% binary noise validation data predicted model (using validation data input
% and optimized parameters)
TS1_bn_val_pred = Tmod_val_bn(:,1);
TS2_bn_val_pred = Tmod_val_bn(:,2);

% normalize validation data to same initial conditions
ICdiff_1 = TS1_bn_val(1) - Tmod1_S1(1); %difference in initial conditions
TS1_bn_val(:) = TS1_bn_val(:) - ICdiff_1;
TS1_bn_val_pred(:) = TS1_bn_val_pred(:)- ICdiff_1;

ICdiff_2 = TS2_bn_val(1) - Tmod1_S2(1);
TS2_bn_val(:) = TS2_bn_val(:) - ICdiff_2;
TS2_bn_val_pred = TS2_bn_val_pred(:)- ICdiff_2;

% goodness of fits 
cost_func = 'MSE';  % mean squared error
fit_test_1 = goodnessOfFit(Tmod1_S1,Treal1_S1,cost_func); 
fit_test_2 = goodnessOfFit(Tmod1_S2,Treal1_S2,cost_func); 
fit_val_1 = goodnessOfFit(TS1_bn_val_pred,TS1_bn_val,cost_func); % compare validation data predicted and actual output  
fit_val_2 = goodnessOfFit(TS2_bn_val_pred,TS2_bn_val,cost_func); 

% plotting results 
t=1:1:900; % time vector 

figure (1)
subplot(221) % input 1
plot(t,Q1_bn_val)
legend("test data","validation data")
subplot(222) % input 2
plot(t,Q2_bn_val), hold on
legend("test data","validation data")
subplot(223) % TS1
plot(t,TS1_bn_val,'Linewidth', 2) % actual output 
plot(t,TS1_bn_val_pred,'Linewidth', 2) % predicted output
legend('model, optimized','model, initial','test data', 'validation data actual output', 'validation data predicted output')
title(['MSE of test data = ',num2str(fit_test_1), ' , MSE of validation data = ',num2str(fit_val_1)])
subplot(224) % TS2
plot(t,TS2_bn_val,'Linewidth', 2) % actual output 
plot(t,TS2_bn_val_pred,'Linewidth', 2) % predicted output
legend('model, optimized','model, initial','test data', 'validation data', 'validation data predicted output')
title(['MSE of test data = ',num2str(fit_test_2), ' , MSE of validation data = ',num2str(fit_val_2)])

%% Validation of multisine  


% multisine test data 
Treal2_S1=data1(:,3);
Treal2_S2=data1(:,4);
% multisine model 
Tmod2_S1=Tmod2(:,1);
Tmod2_S2=Tmod2(:,2);
% multisine validation data 
Q1_ms_val = data6(:,1);
Q2_ms_val = data6(:,2);
TS1_ms_val = data6(:,3);
TS2_ms_val = data6(:,4);
% multisine validation data predicted model (using validation data input
% and optimized parameters)
TS1_ms_val_pred = Tmod_val_ms(:,1);
TS2_ms_val_pred = Tmod_val_ms(:,2);

% normalize validation data to same initial conditions
ICdiff_1 = TS1_ms_val(1) - Tmod2_S1(1); %difference in initial conditions
TS1_ms_val(:) = TS1_ms_val(:) - ICdiff_1;
TS1_ms_val_pred(:) = TS1_ms_val_pred(:)- ICdiff_1;

ICdiff_2 = TS2_ms_val(1) - Tmod2_S2(1);
TS2_ms_val(:) = TS2_ms_val(:) - ICdiff_2;
TS2_ms_val_pred = TS2_ms_val_pred(:)- ICdiff_2;

% goodness of fits 
cost_func = 'MSE';  % mean squared error
fit_test_1 = goodnessOfFit(Tmod2_S1,Treal2_S1,cost_func); 
fit_test_2 = goodnessOfFit(Tmod2_S2,Treal2_S2,cost_func); 
fit_val_1 = goodnessOfFit(TS1_ms_val_pred,TS1_ms_val,cost_func); % compare validation data predicted and actual output  
fit_val_2 = goodnessOfFit(TS2_ms_val_pred,TS2_ms_val,cost_func); 
% plotting results 
t=1:1:900; % time vector 

figure (2)
subplot(221) % input 1
plot(t,Q1_ms_val)
legend("test data","validation data")
subplot(222) % input 2
plot(t,Q2_ms_val), hold on
legend("test data","validation data")
subplot(223) % TS1
plot(t,TS1_ms_val,'Linewidth', 2) % actual output 
plot(t,TS1_ms_val_pred,'Linewidth', 2) % predicted output
legend('model, optimized','model, initial','test data', 'validation data actual output', 'validation data predicted output')
title(['MSE of test data = ',num2str(fit_test_1), ' , MSE of validation data = ',num2str(fit_val_1)])
subplot(224) % TS2
plot(t,TS2_ms_val,'Linewidth', 2) % actual output 
plot(t,TS2_ms_val_pred,'Linewidth', 2) % predicted output
legend('model, optimized','model, initial','test data', 'validation data', 'validation data predicted output')
title(['MSE of test data = ',num2str(fit_test_2), ' , MSE of validation data = ',num2str(fit_val_2)])


%% Make Linearized model
% multisine sequence has the smallest MSE for the validation data, so we
% continue with these parameters, this corresponds to par_opt_2 and data2
t = 1:1:900; % time vector 
eq = Ta; % linearization point at 23 degrees
[A,B,C,D]=linearize_white(par_fix,par_opt_2,eq); % obtain system matrices
sys = ss(A,B,C,D); 
x0 = [23;23;23;23]'; % choose same initial conditions
u = [data2(:,1) data2(:,2)]'; % give a binary noise input 
y = lsim(sys,u,t,x0); % obtain output 
% extract outputs 
T_lin_S1 = y(:,1) + eq; % add equilibrium again because after linearization it is normalized to zero (at eq point)
T_lin_S2 = y(:,2) + eq;

% add to second plot 
figure (2)
subplot(223) % TS1
plot(t,T_lin_S1,'Linewidth', 2) % linearized output 
legend('model, optimized','model, initial','test data', 'validation data actual output', 'validation data predicted output', 'linearized model')
subplot(224) % TS2
plot(t,T_lin_S2, 'Linewidth', 2) % linearized output 
legend('model, optimized','model, initial','test data', 'validation data actual output', 'validation data predicted output', 'linearized model')

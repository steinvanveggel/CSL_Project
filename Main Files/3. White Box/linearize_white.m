function [Am,Bm,Cm,Dm]=linearize(p_fix,p_opt,eq)
% symbolic variables 
syms T_H1d T_H2d T_S1d T_S2d real
syms T_H1 T_H2 T_S1 T_S2 real
syms Q1 Q2 real

%% parameters 
 % fixed parameters
A = p_fix(1); 
As = p_fix(2); 
m = p_fix(3);
sigma = p_fix(4); 
%optimized
Ta = p_opt(1); 
alpha1 = p_opt(2); 
alpha2 = p_opt(3);
tau = p_opt(4);
Cp = p_opt(5);
U = p_opt(6);
Us = p_opt(7);
eps = p_opt(8);

%% define differential equations
% coupling terms
Qc12 = U*As*(T_H2-T_H1);
Qr12  = eps*sigma*A * (T_H2^4 - T_H1^4);
% ODEs
T_H1d = (1.0/(m*Cp))*(U*A*(Ta-T_H1) + (eps * sigma * A * (Ta^4 - T_H1^4)) + Qc12 + Qr12 + alpha1*Q1);
T_H2d = (1.0/(m*Cp))*(U*A*(Ta-T_H2) + (eps * sigma * A * (Ta^4 - T_H2^4)) - Qc12 - Qr12 + alpha2*Q2);
T_S1d = (1/tau)*(T_H1-T_S1);
T_S2d = (1/tau)*(T_H2-T_S2);

%% state, input and output definition
x = [T_H1 T_H2 T_S1 T_S2]';
xdot = [T_H1d T_H2d T_S1d T_S2d]';
u = [Q1 Q2]';
y = [T_S1 T_S2]';

%% linearize 
Am = jacobian(xdot,x);
Bm = jacobian(xdot,u);
Cm = jacobian(y,x);
Dm = jacobian(y,u);
% fill in equilibrium point
T_H1 = eq;
T_H2 = eq; 
T_S1 = eq; 
T_S2 = eq;
% obtain system matrices  
Am = eval(Am);
Bm = eval(Bm);
Cm = eval(Cm);
Dm = eval(Dm);
end 
%% discretization of system 
% A = [-0.0070    0.0010         0         0;
%     0.0010   -0.0070         0         0;
%     0.0417         0   -0.0417         0;
%          0    0.0417         0   -0.0417];
%      
% B =    [0.0051         0;
%          0    0.0038;
%          0         0;
%          0         0];
%      
% C =  [0     0     1     0;
%      0     0     0     1];
%  
%  D = zeros(2,2);

G = ss(A,B,C,D); %create ss model from continuous system matrices
dt = 1; %sample time
H = c2d(G,dt,'zoh'); %discretise with zero order hold
[Ad,Bd,Cd,Dd] = ssdata(H); % obtain discrete system matrices 
%% pole placement
Mp = 0.001; %  max overshoot in degrees celcius
Ts = 800; % settling time in seconds 

zeta = -log(Mp)/(sqrt(pi^2+log(Mp)^2)) % damping constant 
omega = 4/(Ts*zeta) % eigenfrequency 

p1 = -2*exp(-zeta*omega*dt)*cos(omega*dt*sqrt(1-zeta^2));
p2 = exp(-zeta*omega*dt);

p = [1 p1 p2]; % put p1 and p2 in desired polynomial z^2 + p1*z + p2 
poles = roots(p); % calculate poles 

p_C = [-omega; -5*omega;-omega;-5*omega]; % controller poles 
p_O = 0.01*p_C; % observer poles, 10 times faster 

L = place(A',C',p_O)'; % Observer gain
K = place(A,B,p_C); % Controller gain


%% compute feedforward gain 
% assume steady state, so Ax + Bu = 0
% so x_ss = -inv(A)*B*u
% so y_ss = C*x_ss = -C*inv(A)*B*u
% for ff we have u = -K*x + Kr*r, and we want r = y, so u = -K*x_ss+Kr*y_ss
% filling in x_ss and y_ss gives u = -K*(-inv(A)*B*u) + Kr*-C*inv(A)*B*u
% so Kr =  (K*inv(A)*B-eye(2))*inv(C*inv(A)*B)
% in discrete time Kr = (K*inv(Ad)*Bd-eye(2))*inv(Cd*inv(Ad)*Bd)

Kr = -inv(C*inv(A-B*K)*B);

%% test if we get logical input values
% equilibrium point is 23 degrees, lets say it is now 21 and we want to get
% it to 24 (so reference = 1)
x = 23; % our steady state is 23, so 21 degrees means -2 from our steady state value 
r = 40; % 1 above steady state
Xtest = [x x x x]';
Rtest = [r r]';

Utest_ss = -K*Xtest
Utest_ref = -K*Xtest + Kr*Rtest

% %% Plot the State Estimation and Reference Tracking
% t=1:1:900
% figure (1)
% subplot(223) % TS1
% plot(t,error_state(:,1),t,error_state(:,2)) % State Estimation 
% legend('Sensor-1','Sensor-2')
% subplot(224) % TS2
% plot(t,ref_error(:,1),t,ref_error(:,2)) % Reference Tracking
% legend('Sensor-1','Sensor-2')




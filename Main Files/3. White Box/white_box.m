function [Tmod, par_opt] = white_box(data,par_int)
%% Constants 
% Fixed Parameters
A = 12 / 100^2; 
As = 2.0 / 100.0^2; 
m = 4 / 1000;
sigma = 5.67e-8; 
% To be optimized
Ta = par_int(1); 
alpha1 = par_int(2); 
alpha2 = par_int(3);
tau = par_int(4);
Cp = par_int(5);
U = par_int(6);
Us = par_int(7);
eps = par_int(8);

%% Parameters
p_fix = [A As m sigma]; % Fixed parameters
p_int = [Ta alpha1 alpha2 tau Cp U Us eps]; % Initial parameter values, to be optimized 
p_low = zeros(1,8); % Lower bound
p_upp = 10*p_int; % Upper bound

%% lsqnonlin Algorithm 
func = @(p)errfun(p,p_fix,data); % error function
options = optimset('lsqnonlin');
options = optimset(options, 'Display','iter');
% Obtain optimized Parameters - lsqnonlin
% Calculate final Tmod
[p_opt,V2,~,~,output2] = lsqnonlin(func,p_int,p_low,p_upp,options); 
[~,Tmodint] = errfun(p_int,p_fix,data); % Tmod with initial parametervector
[~,Tmod] = errfun(p_opt,p_fix,data); % Tmod with optimized parametervector 

%% Extract Outputs for Plotting
% Tmod with initial Parameters
Tmodint_S1 = Tmodint(:,3); 
Tmodint_S2 = Tmodint(:,4);
% Tmod with optimized Parameters
Tmod_S1 = Tmod(:,3);  
Tmod_S2 = Tmod(:,4);
% T from actual data
Treal_S1 = data(:,3);
Treal_S2 = data(:,4);
% Q from data
Q1 = data(:,1); 
Q2 = data(:,2);
% Observation Time
t = 1:1:900;
%% Evaluate Goodness of Fit 
cost_func = 'MSE';  % mean squared error
fit_1 = goodnessOfFit(Tmod_S1,Treal_S1,cost_func); % For nonlinear model
fit_2 = goodnessOfFit(Tmod_S2,Treal_S2,cost_func); 
%% Plot Results
figure
subplot(221) % input 1
plot(t,Q1), hold on
xlabel('time (s)','Fontsize',14)
ylabel('input Q1 (%)','Fontsize',14)
ylim([0 100]);
subplot(222) % input 2
plot(t,Q2), hold on
xlabel('time (s)','Fontsize',14)
ylabel('input Q2 (%)','Fontsize',14)
ylim([0 100]);
subplot(223) % TS1
plot(t,Tmod_S1,'LineWidth',2), hold on
plot(t,Tmodint_S1,'LineWidth',1)
plot(t,Treal_S1,'LineWidth',1)
xlabel('time (s)','Fontsize',14)
ylabel('TS1 (celcius)','Fontsize',14)
ylim([0 80]);
legend('TS1_{model,nonlinear}','TS1_{model,initial}','TS1_{real}')
title(['MSE of nonlinear fit = ',num2str(fit_1)])
subplot(224) % TS2
plot(t,Tmod_S2,'LineWidth',2), hold on
plot(t,Tmodint_S2,'LineWidth',1)
plot(t,Treal_S2,'LineWidth',1)
xlabel('time (s)','Fontsize',14)
ylabel('TS2 (celsius)','Fontsize',14)
ylim([0 80]);
legend('TS2_{model,nonlinear}','TS2_{model,initial}','TS2_{real}')
title(['MSE of nonlinear fit = ',num2str(fit_2)])

%% return parameters
par_opt = p_opt;
end 




    
    
    



clc
clear all
%% Loading the data for Black-Box identification(Linear)
load prbs_data_1800.csv
load prbs_data_1800val.csv
load data_exp_bn1.csv
load data_exp_ms1.csv
data1 = prbs_data_1800;
data_val1 = prbs_data_1800val;
%% Black Box Model Estimation
[model_1,sys_tf] = black_box(data1,data1);
figure(1)
sgtitle('Input Signal, Black-Box Modelling')
figure(2)
sgtitle('Experiment-Estimation, ARMAX Model')

%% Validation of Black-Box
val_1 = black_box(data1,data_val1);
figure(3)
sgtitle('Input Signal, Black-Box Modelling')
figure(4)
sgtitle('Experiment-Validation, ARMAX Model')

%% Step Response
figure(5)
step(model_1)
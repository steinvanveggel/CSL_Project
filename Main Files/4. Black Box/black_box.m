function [model,tf] = black_box(data,validate)
t = 1:1:length(data);
x0=[23,23,23,23]';
%% Splitting data for training and testing
train = data(:,:);
test = validate(:,:);
%% Processing the training data
Q1 = train(:,1);
Q2 = train(:,2);
T_S1 = train(:,3) - train(1,3);
T_S2 = train(:,4) - train(1,3);
train_data = iddata([T_S1,T_S2],[Q1,Q2],1);
train_data.InputName  = {'Heater 1 Power';'Heater 2 Power'};
train_data.OutputName = {'Sensor 1 Temperature';'Sensor 2 Temperature'};
%% Black-Box Identification
% Define the ARMAX Model - Training
na = [2 3;3 2];
nb = [2 3;3 2];
nc = [1;1];
nk = ones(2);
model_est = armax(train_data,[na nb nc nk]);
%Converting ARMAX model to statespace model
sys_ss = idss(model_est);
%Converting ARMAX model to transfer function model
sys_tf = idtf(model_est);
% Retrieving the data as u(Input) and y(Output)
u_train = train_data.u;
y_train = train_data.y;

%% Testing data Processing
Q1_test = test(:,1);
Q2_test = test(:,2);
T_S1_test = test(:,3)-test(1,3);
T_S2_test = test(:,4)-test(1,4);
test_data = iddata([T_S1_test,T_S2_test],[Q1_test,Q2_test],1);
test_data.InputName  = {'Heater 1 Power';'Heater 2 Power'};
test_data.OutputName = {'Sensor 1 Temperature';'Sensor 2 Temperature'};
u_test = test_data.u;
y_test = test_data.y;
%% Simulating the model using the test input u_test
y_model = lsim(model_est,u_test);
y_model_ss  = lsim(sys_ss,u_test); % State Space Model
y_model_tf = lsim(sys_tf,u_test);  % Transfer Function Model

%% Mean-Squared Error and Goodness of Fit
Tmod_S1 = y_model(:,1);
Tmod_S2 = y_model(:,2);
Treal_S1 = y_test(:,1);
Treal_S2 = y_test(:,2);
cost_func = 'MSE';  % Mean Squared Error
fit_bb_S1 = goodnessOfFit(Tmod_S1,Treal_S1,cost_func);
fit_bb_S2 = goodnessOfFit(Tmod_S2,Treal_S2,cost_func); 
%% Plotting the Real and validation data
figure()
% Input 1 Q1
subplot(211) 
plot(t,Q1_test)
xlabel('time (s)','Fontsize',10)
ylabel('Input Q1 (%)','Fontsize',10)
ylim([0 100]);


% Input 2 Q2
subplot(212) 
plot(t,Q2_test)
xlabel('time (s)','Fontsize',10)
ylabel('Input Q2 (%)','Fontsize',10)
ylim([0 100]);


figure()
% Temperature 1 T_S1
subplot(211)
plot(t,y_test(:,1),'LineWidth',2), hold on
plot(t,y_model(:,1),'LineWidth',2)
xlabel('Time (s)','Fontsize',10)
ylabel('Sensor Temperature-1 [^{o} C]','Fontsize',10)
legend('TS1_{test,Real}','TS1_{model,ARMAX}')
xlim([0 1800]);
title(['MSE of Linear fit = ',num2str(fit_bb_S1)])

% Temperature 2 T_S2
subplot(212)
plot(t,y_test(:,2),'LineWidth',2), hold on
plot(t,y_model(:,2),'LineWidth',2)
xlabel('Time (s)','Fontsize',10)
ylabel('Sensor Temperature-2 [^{o} C]','Fontsize',10)
legend('TS2_{test,Real}','TS2_{model,ARMAX}')
xlim([0 1800]);
title(['MSE of Linear fit = ',num2str(fit_bb_S2)])

model=model_est;
tf = sys_tf;
% compare(model_est,test_data,sys_ss,sys_tf);
end




clear; close all; clc

% Observation Time
Ts = 900;
%Sample Time = 1
t = 1:Ts;
t=t';

% %The Inputs given 
% 1.[u,freq] = idinput([900 2 1],'prbs',[0,1/8],[0,40]);
% 2.[u,freq] = idinput([900 2 1],'sine',[0,1/8],[0, 40]);
% 3.for i = 1:900
%   if i >=1 && i<=100
%        u1(i)=0;
%        u2(i)=0;
%    end
%     if i>100&& i <= 300
%         u1(i) = 20;
%         u2(i) = 20;
%     end
%     if i>300 && i <=500
%         u1(i) = 40;
%         u2(i) = 40;
%     end
%     if i>500 && i <=700
%         u1(i) = 20;
%         u2(i) = 20;
%     end
%     if i>700 && i <=900
%         u1(i) = 0;
%         u2(i) = 0;
%     end  
%     end
%% Generating a sum-of-sinusoids signal
for i = 1:900
   if i >=1 && i<=100
       u2(i)=40;
       u1(i)=0;
   end
    if i>100&& i <= 300
        u2(i) = 40;
        u1(i) = 20;
    end
    if i>300 && i <=500
        u2(i) = 40;
        u1(i) = 40;
    end
    if i>500 && i <=700
        u2(i) = 40;
        u1(i) = 20;
    end
    if i>700 && i <=900
        u2(i) = 40;
        u1(i) = 0;
    end  
end
Q1 = [t , u1'];
Q2 = [t , u2'];
sim('heater.slx');

%% Plot the Input(Power) and Output(Sensor Temperatures)
figure(1)
subplot(211)
plot(tout,T_S1,'LineWidth',2), hold on
plot(tout,T_S2,'LineWidth',2)
xlabel('time (s)','Fontsize',14)
ylabel('state','Fontsize',14)
legend('T_S1','T_S2')
subplot(212)
plot(u1,'LineWidth',2), hold on
plot(u2,'LineWidth',2)
xlabel('time (s)','Fontsize',14)
ylabel('input','Fontsize',14)
legend('u1','u2')

%% Saving the input and output data in a spreadsheet
% Q1 and Q2 is 0 at time = 0
u1 = [0;u1(:,:)'];
u2 = [0;u2(:,:)'];
data = [u1,u2,T_S1,T_S2];
csvwrite('data_sim_step1.csv',data)
save('data_sim_step1.mat','data')

%% Experiment Data
clc 
clear all
load data_exp_bn1.csv
exp_data = data_exp_bn1;
save('data_exp_bn1.mat','exp_data')
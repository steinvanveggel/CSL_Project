%% discretization of system 
G = ss(A,B,C,D); %create ss model from continuous system matrices
dt = 1; %sample time
H = c2d(G,dt); %discretise with zero order hold
[Ad,Bd,Cd,Dd] = ssdata(H); % obtain discrete system matrices 

%% pole definitions
% desired rise time in seconds
tau_H = 500; 
tau_S = 501; % note: different tau because otherwise algebraic multiplicity of poles
% controller poles

p_C = [-0.005; -0.005;-0.02; -0.02]; %NOT SURE if still case after c2d
% observer poles, we will take these 10 times as fast as the controller
p_O = p_C*10;


L = place(Ad',Cd',p_O)'; % Observer gain
K = place(Ad,Bd,p_C); % Controller gain
Kr = -inv(Cd*inv(Ad-Bd*K)*Bd); % Reference Gain


%% Discrete Observer
Aobs = [Ad-L*Cd];
Bobs = [Bd L];
Cobs = Cd;
Dobs = zeros(4,4);
%% LQR

Q = eye(4);
R = 10*eye(2);
K_lqr = dlqr(A,B,Q,R); 

